import React from 'react';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import {DefaultPage, Home, Login, Register} from './page/Index';
import './../css/App.css';

const App: React.FC = () => 
{
    return(
        <div className="App">
            <Router>
                <Switch>
                    <Route exact path="/">
                        <DefaultPage Page={Home}/>
                    </Route>
                    <Route path="/login">
                        <DefaultPage Page={Login}/>
                    </Route>
                    <Route path="/register">
                        <DefaultPage Page={Register}/>
                    </Route>
                </Switch>
            </Router>
        </div>
    )
}
export default App;