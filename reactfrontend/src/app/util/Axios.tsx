import axios from 'axios';

class Axios {
  private baseURL = 'http://localhost:8080/';

  public async registerReq (email: string, pseudo: string, password: string) {
    const data = await axios({
      method: 'post',
      url: this.baseURL + 'register',
      data: {
        email: email,
        pseudo: pseudo,
        password: password
      }
    });
    return data;
  }

  public async loginReq (email: string, password: string) {
    const data = await axios({
      method: 'post',
      url: this.baseURL + 'authentication',
      data: {
        email: email,
        password: password
      }
    });
    return data
  }

  public async emailValidity (email: string) {
    const data = await axios({
      method: 'get',
      url: this.baseURL + 'emailValidity/' + email,
    });
    return data.data;
  }
  public async pseudoValidity (pseudo: string) {
    const data = await axios({
      method: 'get',
      url: this.baseURL + 'pseudoValidity/' + pseudo,
    });
    return data.data;
  }
}

export default new Axios();


