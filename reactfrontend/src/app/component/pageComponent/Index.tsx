export { default as Footer } from './Footer';
export { default as NavigationBar} from './NavigationBar';
export { default as FormRegister} from './FormRegister';
export { default as FormLogin} from './FormLogin';
