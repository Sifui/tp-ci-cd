import React, { useState } from 'react';
import {Button, InputText} from '../coreComponent/Index';
import axios from '../../util/Axios';
import { useHistory } from "react-router-dom";

const FormRegister: React.FC = () => 
{
	const LOCAL_INFO_TYPE = { text: '', localValidity: true };
	const SERVER_INFO_TYPE = { text: '', localValidity: true, serverValidity: false };
	const [email, setEmail] = useState(SERVER_INFO_TYPE);
	const [pseudo, setPseudo] = useState(SERVER_INFO_TYPE);
	const [pwd, setPwd] = useState(LOCAL_INFO_TYPE);
	const [pwdVerif, setPwdVerif] = useState(LOCAL_INFO_TYPE);
	const [isRegister, setIsRegister] = useState(false);
	const history = useHistory();

	const handleSubmit = async () => 
	{
		try 
		{
			const res = await axios.registerReq(email.text, pseudo.text, pwd.text);
			console.log(res);
			if (res) 
			{
				console.log("registered")
				setIsRegister(true);
			} 
			else 
			{
				console.log("can't register");
			}		
		} 
		catch (e) 
		{
			if (e.response) 
			{
				console.log(e.response);
				alert("Email ou pseudo déjà existant");
			} 
			else if (e.request) 
			{
				console.log(e.request);
				alert("Pas de réponse du serveur, veuillez réessayer plus tard");
			}
			else 
			{
				console.log(e);
				alert("Erreur inconnue");
			}
		}
	}
	
	const handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => 
	{
		setEmail({
			text: e.currentTarget.value,
			localValidity: email.localValidity,
			serverValidity: email.serverValidity
		})
	}
	const handlePseudoChange = (e: React.ChangeEvent<HTMLInputElement>) => 
	{
		setPseudo({
			text: e.currentTarget.value,
			localValidity: pseudo.localValidity,
			serverValidity: pseudo.serverValidity
		})
	}
	const handlePwdChange = (e: React.ChangeEvent<HTMLInputElement>) => 
	{
		setPwd({
			text: e.currentTarget.value,
			localValidity: pwd.localValidity
			});
	}
	const handlePwdVerifChange = (e: React.ChangeEvent<HTMLInputElement>) => 
	{
		setPwdVerif({
			text: e.currentTarget.value,
			localValidity: pwdVerif.localValidity
		})
	}

	const emailCheck = async (e: React.FocusEvent<HTMLInputElement>) => 
	{
		let localValidity = emailVerification(email.text);
		let serverValidity: boolean = false;
	 	if (localValidity && email.text !== "") 
	 	{
	 	 	serverValidity = await axios.emailValidity(email.text)
		}
		setEmail({
			text: email.text,
			localValidity: localValidity,
			serverValidity: serverValidity
		})
	}

	const pseudoCheck = async (e: React.FocusEvent<HTMLInputElement>) => 
	{
		let localValidity = pseudoVerification(pseudo.text);
		let serverValidity: boolean = false;
	 	if (localValidity && pseudo.text !== "") 
	 	{
			serverValidity = await axios.pseudoValidity(pseudo.text)
		}
		setPseudo({
			text: pseudo.text,
			localValidity: localValidity,
			serverValidity: serverValidity
		})
	}

	const pwdCheck = async (e: React.FocusEvent<HTMLInputElement>) => 
	{
		setPwd({
			text: pwd.text,
			localValidity: passwordVerification(pwd.text),
		})
	}

	const pwdVerifCheck = async (e: React.FocusEvent<HTMLInputElement>) => 
	{
		setPwdVerif({
			text: pwdVerif.text,
			localValidity: passwordMatch(pwdVerif.text, pwd.text),
		})
	}

	const correctInputs = () => 
	{
		if (email.localValidity && email.text !== "" 
		&& pseudo.localValidity && pseudo.text !== "" 
		&& pwd.localValidity && pwd.text !== "" 
		&& pwdVerif.localValidity && pwdVerif.text !== "" 
		&& email.serverValidity && pseudo.serverValidity) 
		{ 
			return <Button className="ButtonDefault2" disabled={false} onClick={handleSubmit}>Confirmer</Button>
		}
		else
		{
			return <Button className="ButtonDefault2NotClickable" disabled={true} onClick={handleSubmit}>Confirmer</Button>
		}
	}

	const correctEmail = () =>
	{
		return(
			<>
				<InputText
					className={email.localValidity || email.text === "" ? `ImputText` : `ImputTextError`}
					placeholder="Email"
					value={email.text}
					type="text"
					onChange={handleEmailChange}
					onBlur={emailCheck}
				/>
				{
					email.localValidity || email.text === "" ? <></>
					// :email.localValidity && email.text !== "" && !email.serverValidity ? <span className="ErrorMessage">Cette adresse e-amil est déjà utilisé</span> 
					: <span className="ErrorMessage">Veuillez entrer une adresse e-mail valide</span>
				}
			</>
		)
	}

	const correctPseudo = () =>
	{
		return(
			<>
				<InputText
					className={email.localValidity || email.text === "" ? `ImputText` : `ImputTextError`}
					placeholder="Pseudonyme"
						value={pseudo.text}
						type="text"
						onChange={handlePseudoChange}
						onBlur={pseudoCheck}
				/>
				{
					pseudo.localValidity || pseudo.text === "" ? <></>
					// :pseudo.localValidity && pseudo.text !== "" && !pseudo.serverValidity ? <span className="ErrorMessage">Ce pseudonyme est déjà utilisé</span>
					: <span className="ErrorMessage">Le pseudonyme doit faire de 6 à 30 caractères</span>
				}
			</>
		)
	}

	const correctPwd = () =>
	{
		return(
			<>
				<InputText
					className={pwd.localValidity || pwd.text === "" ? `ImputText` : `ImputTextError`}
					placeholder="Mot de passe"
					value={pwd.text}
					type="password"
					onChange={handlePwdChange}
					onBlur={pwdCheck}
				/>
				{
					pwd.localValidity || pwd.text === "" ? <></>
					: <span className="ErrorMessage">
						• Le mot de passe doit faire de 8 à 30 caractères<br/>
						• Le mot de passe doit contenir au moins une lettre minuscule<br/>
						• Le mot de passe doit contenir au moins une lettre majuscule<br/>
						• Le mot de passe doit contenir au moins un chiffre<br/>
						• Le mot de passe doit contenir au moins un caractère spécial
					</span>
				}
			</>
		)
	}

	const correctPwdVerif = () =>
	{
		return(
			<>
				<InputText
					className={pwdVerif.localValidity || pwdVerif.text === "" ? `ImputText` : `ImputTextError`}
					placeholder="Vérification de mot de passe"
					value={pwdVerif.text}
					type="password"
					onChange={handlePwdVerifChange}
					onBlur={pwdVerifCheck}
				/>
				{
					pwdVerif.localValidity || pwdVerif.text === "" ? <></>
					: <span className="ErrorMessage">Ne correspond pas au champ «mot de passe»</span>
				}
			</>
		)
		
	}

	return (
		<>
		{ !isRegister
			? 
			<>
				<div className="TittleContainerRegister">
					Inscription
				</div>
				<div className="ContainerRegister">
					{correctEmail()}
				</div>

				<div className="ContainerRegister">
					{correctPseudo()}
				</div>

				<div className="ContainerRegister">
					{correctPwd()}
				</div>

				<div className="ContainerRegister">
					{correctPwdVerif()}
				</div>

				<div className="ContainerRegister">
					{correctInputs()}
				</div>
			</>
			: 
			<>
				<div className="TittleContainerRegister">
					Votre compte a été créé avec succès
				</div>
				<div className="ContainerRegister">
					<Button className="ButtonDefault2" onClick={() => history.push("/Login")}>Se connecter</Button>
				</div>
			</>
		}
		</>

	);
}

function emailVerification (email: string) {
	const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}

function pseudoVerification (pseudo: string) {
	if (pseudo.length < 6 || pseudo.length > 30) return false;
	return true;
}

function passwordVerification (password: string) {
	const re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,30}$/;
	return re.test(String(password))
}

function passwordMatch(pwdVerif: string, pwd: string) {
	if (pwdVerif === pwd) return true;
	return false;
}

export default FormRegister;