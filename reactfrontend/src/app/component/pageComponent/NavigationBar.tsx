import React, {useState} from 'react';
import { useHistory } from "react-router-dom";
import {Button, InputText} from '../coreComponent/Index';
import{WindowSizeHook} from './../../util/Index';
import './../../../css/NavigationBar.css';

interface Props{}

const NavigationBar: React.FC<Props> = () => 
{
    const history = useHistory();
    const { width } = WindowSizeHook();
    const breakpoint = 1200;
    let [sidebar, setSidebar] = useState(false);

    const ClickSidebar = () => {setSidebar(!sidebar)}

    if(width < breakpoint)
    {
        return(
            <div className="NavigationBar">
                
                {/* Side bar */}
                <nav className={sidebar ? 'Sidebar active' : 'Sidebar'}>
                    
                    {/* Toogle side bar button*/}
                    <div className="SideBarContainer">
                        <Button className="ButtonTransparent" onClick={ClickSidebar}>
                            <i className="fa fa-bars" aria-hidden="true"></i>
                        </Button>
                    </div>

                    {/* Create a guide button*/}
                    <div className="SideBarContainer">
                        <Button className="ButtonTransparent">
                            Créer un guide
                        </Button>
                    </div>

                    {/* Search a guide button*/}
                    <div className="SideBarContainer">
                        <Button className="ButtonTransparent">
                            Recherche un guide
                        </Button>
                    </div>

                    {/* Search a Kovaak playlist button*/}
                    <div className="SideBarContainer">
                        <Button className="ButtonTransparent">
                            Playlist Kovaak
                        </Button>
                    </div>

                </nav>

                {/* Navigation bar first row*/}
                <div className="NavigationBarContainer">

                    {/* Toogle side bar button*/}
                    <div className="ButtonContainerNavigationBar">
                        <Button className="ButtonTransparent" onClick={ClickSidebar}>
                            <i className="fa fa-bars" aria-hidden="true"></i>
                        </Button>
                    </div>

                    {/* Home button with logo picture */}
                    <div className="LogoContainerNavigationBar">
                        <Button className="ButtonImage" onClick={() => history.push("/")}>
                            <img className="LogoNavigationBar"src="./image/Logo.png" alt="logo du site tryhard ninja"/>
                        </Button>
                    </div>

                    {/* Login button */}
                    <div className="ButtonContainerNavigationBar">
                        <Button className="ButtonDefault" onClick={() => history.push("/login")}>
                            Connexion
                        </Button>
                    </div>

                </div>

                {/* Navigation Bar second row */}
                <div className="NavigationBarContainer">

                    {/* Searchbar that search everything */}
                    <div className="SearchBarContainerNavigationBar">
                        <div className="SearchBarNavigationBar">
                            <InputText className="InputSearchBarDarkTheme"  placeholder="Rechercher"/>
                            <Button className="ButtonSearchBarDarkTheme">
                                <i className="fa fa-search" aria-hidden="true"></i>
                            </Button>
                        </div>
                    </div>

                </div>

            </div>
        )
    }
    else
    {
        return(
            <div className="NavigationBar">
                
                {/* Home button with logo picture */}
                <div className="LogoContainerNavigationBar">
                    <Button className="ButtonImage" onClick={() => history.push("/")}>
                        <img className="LogoNavigationBar"src="./image/Logo.png" alt="logo du site tryhard ninja"/>
                    </Button>
                </div>
                
                {/* Create a guide button*/}
                <div className="ButtonContainerNavigationBar">
                    <Button className="ButtonTransparent">
                        Créer un guide
                    </Button>
                </div >

                {/* Search a guide button*/}
                <div className="ButtonContainerNavigationBar">
                    <Button className="ButtonTransparent">
                        Recherche un guide
                    </Button>
                </div>

                {/* Search a Kovaak playlist button*/}
                <div className="ButtonContainerNavigationBar">
                    <Button className="ButtonTransparent">
                        Playlist Kovaak
                    </Button>
                </div>

                {/* Searchbar that search everything */}
                <div className="SearchBarContainerNavigationBar">
                    <div className="SearchBarNavigationBar">
                        <InputText className="InputSearchBarDarkTheme"  placeholder="Rechercher"/>
                        <Button className="ButtonSearchBarDarkTheme">
                            <i className="fa fa-search" aria-hidden="true"></i>
                        </Button>
                    </div>
                </div>

                {/* Login button */}
                <div className="ButtonContainerNavigationBar">
                    <Button className="ButtonDefault" onClick={() => history.push("/login")}>
                        Connexion
                    </Button>
                </div>

            </div>
        )
    }
}
export default NavigationBar;