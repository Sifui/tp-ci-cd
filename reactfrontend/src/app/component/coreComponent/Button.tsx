import React from "react";
import './../../../css/CoreComponent.css';

interface Props{
    disabled?: boolean
}

const Button: React.FC<React.HTMLAttributes<HTMLButtonElement> & Props> = 
(
    {
        children,
        onClick,
        className,
        disabled
    }
) => 
{
    return(
        <button 
        disabled={disabled}
        className={className}
        onClick={onClick}
        >
            {children}
        </button>
    )
}
export default Button;