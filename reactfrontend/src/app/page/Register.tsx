import React from 'react';
import {FormRegister} from '../component/pageComponent/Index';
import './../../css/Register.css';

interface Props{}

const Register: React.FC<Props> = () => 
{
    return(
        <div className="Register">
            <FormRegister/>
        </div>
    )
}
export default Register;