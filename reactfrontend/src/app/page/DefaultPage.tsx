import React from 'react';
import {NavigationBar, Footer} from '../component/pageComponent/Index';

interface Props
{
    Page:  React.FC;
}

const DefaultPage: React.FC<Props> = 
(
    {
        Page
    }
) => 
{
    return(
        <div className="DefaultPage">
            <NavigationBar/>
            <div className="PageContent">
                <Page/>
            </div>
            <Footer/>
        </div>
    )
}
export default DefaultPage;