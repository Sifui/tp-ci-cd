import React from 'react';
import {FormLogin} from '../component/pageComponent/Index';
import './../../css/Login.css';


interface Props{}

const Login: React.FC<Props> = () => 
{
    return(
        <div className="Login">
            <FormLogin/>
        </div>
    )
}
export default Login;