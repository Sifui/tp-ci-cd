package com.intech.tryhardninja.storage.DAODatabase;


import com.intech.tryhardninja.entities.Comment;
import com.intech.tryhardninja.storage.DAO.CommentDAO;
import com.intech.tryhardninja.storage.repositories.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(name = "storagemode", havingValue = "dataBase")
public class CommentDAODatabase implements CommentDAO {
    @Value("${connectionURL}")
    private String connectionURL;

    @Autowired
    private CommentRepository commentRepository;

    public Iterable<Comment> getComment() {
        return commentRepository.findAll();
    }
    @Override
    public void insertComment(Comment comment) {
        commentRepository.save(comment);
    }

    public String getConnectionURL() {
        return connectionURL;
    }

    public void setConnectionURL(String connectionURL) {
        this.connectionURL = connectionURL;
    }
}

