package com.intech.tryhardninja.storage.DAO;

import com.intech.tryhardninja.entities.User;

public interface UserDAO {
    Iterable<User> getUser();
    void insertUser(User user);
    User findUserByEmailPseudo(String email, String pseudo);
    User findUserByEmailPassword(String email, String password);
    Boolean emailValidity(String email);
    Boolean pseudoValidity(String pseudo);
}
