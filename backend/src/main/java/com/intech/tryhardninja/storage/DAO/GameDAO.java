package com.intech.tryhardninja.storage.DAO;

import com.intech.tryhardninja.entities.Game;

public interface GameDAO {
    Iterable<Game> getGame();
    void insertGame(Game game);
}
