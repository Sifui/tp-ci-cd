package com.intech.tryhardninja.storage.DAO;

import com.intech.tryhardninja.entities.Guide;

public interface GuideDAO {
    Iterable<Guide> getGuide();
    void insertGuide(Guide guide);
}
