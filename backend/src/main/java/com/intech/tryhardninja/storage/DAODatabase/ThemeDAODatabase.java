package com.intech.tryhardninja.storage.DAODatabase;


import com.intech.tryhardninja.entities.Theme;
import com.intech.tryhardninja.storage.DAO.ThemeDAO;
import com.intech.tryhardninja.storage.repositories.ThemeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(name = "storagemode", havingValue = "dataBase")
public class ThemeDAODatabase implements ThemeDAO {
    @Value("${connectionURL}")
    private String connectionURL;

    @Autowired
    private ThemeRepository themeRepository;

    public Iterable<Theme> getTheme() {
        return themeRepository.findAll();
    }
    @Override
    public void insertTheme(Theme theme) {
        themeRepository.save(theme);
    }

    public String getConnectionURL() {
        return connectionURL;
    }

    public void setConnectionURL(String connectionURL) {
        this.connectionURL = connectionURL;
    }
}
