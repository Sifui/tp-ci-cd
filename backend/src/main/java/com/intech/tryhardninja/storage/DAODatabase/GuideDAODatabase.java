package com.intech.tryhardninja.storage.DAODatabase;


import com.intech.tryhardninja.entities.Guide;
import com.intech.tryhardninja.storage.DAO.GuideDAO;
import com.intech.tryhardninja.storage.repositories.GuideRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(name = "storagemode", havingValue = "dataBase")
public class GuideDAODatabase implements GuideDAO {
    @Value("${connectionURL}")
    private String connectionURL;

    @Autowired
    private GuideRepository guideRepository;

    public Iterable<Guide> getGuide() {
        return guideRepository.findAll();
    }
    @Override
    public void insertGuide(Guide guide) {
        guideRepository.save(guide);
    }

    public String getConnectionURL() {
        return connectionURL;
    }

    public void setConnectionURL(String connectionURL) {
        this.connectionURL = connectionURL;
    }
}

