package com.intech.tryhardninja.storage.repositories;

import com.intech.tryhardninja.entities.CharacterImage;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterImageRepository extends CrudRepository<CharacterImage, Integer> {
}