package com.intech.tryhardninja.storage.DAO;

import com.intech.tryhardninja.entities.Character;

public interface CharacterDAO {
    Iterable<Character> getCharacter();
    void insertCharacter(Character character);
}
