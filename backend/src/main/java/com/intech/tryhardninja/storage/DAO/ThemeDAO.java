package com.intech.tryhardninja.storage.DAO;

import com.intech.tryhardninja.entities.Theme;

public interface ThemeDAO {
    Iterable<Theme> getTheme();
    void insertTheme(Theme guide);
}
