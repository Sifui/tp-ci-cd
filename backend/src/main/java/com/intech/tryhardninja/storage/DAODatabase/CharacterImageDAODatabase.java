package com.intech.tryhardninja.storage.DAODatabase;


import com.intech.tryhardninja.entities.CharacterImage;
import com.intech.tryhardninja.storage.DAO.CharacterImageDAO;
import com.intech.tryhardninja.storage.repositories.CharacterImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(name = "storagemode", havingValue = "dataBase")
public class CharacterImageDAODatabase implements CharacterImageDAO {
    @Value("${connectionURL}")
    private String connectionURL;

    @Autowired
    private CharacterImageRepository characterImageRepository;

    public Iterable<CharacterImage> getCharacterImage() {
        return characterImageRepository.findAll();
    }
    @Override
    public void insertCharacterImage(CharacterImage characterImage) {
        characterImageRepository.save(characterImage);
    }

    public String getConnectionURL() {
        return connectionURL;
    }

    public void setConnectionURL(String connectionURL) {
        this.connectionURL = connectionURL;
    }
}
