package com.intech.tryhardninja.storage.DAODatabase;

import com.intech.tryhardninja.entities.Character;
import com.intech.tryhardninja.storage.DAO.CharacterDAO;
import com.intech.tryhardninja.storage.repositories.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(name = "storagemode", havingValue = "dataBase")
public class CharacterDAODatabase implements CharacterDAO {
    @Value("${connectionURL}")
    private String connectionURL;

    @Autowired
    private CharacterRepository characterRepository;

    public Iterable<Character> getCharacter() {
        return characterRepository.findAll();
    }
    @Override
    public void insertCharacter(Character character) {
        characterRepository.save(character);
    }

    public String getConnectionURL() {
        return connectionURL;
    }

    public void setConnectionURL(String connectionURL) {
        this.connectionURL = connectionURL;
    }
}
