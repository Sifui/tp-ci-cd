package com.intech.tryhardninja.storage.DAO;

import com.intech.tryhardninja.entities.CharacterImage;


public interface CharacterImageDAO {
    Iterable<CharacterImage> getCharacterImage();
    void insertCharacterImage(CharacterImage characterImage);
}
