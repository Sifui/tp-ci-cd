package com.intech.tryhardninja.storage.repositories;


import com.intech.tryhardninja.entities.Game;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository extends CrudRepository<Game, Integer> {
}
