package com.intech.tryhardninja.storage.repositories;

import com.intech.tryhardninja.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    User findByEmailOrPseudo(String email, String pseudo);
    User findByEmailAndPassword(String email, String password);
    Boolean existsByEmail(String email);
    Boolean existsByPseudo(String pseudo);
}