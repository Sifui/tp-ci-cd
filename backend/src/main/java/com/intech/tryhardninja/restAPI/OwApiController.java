package com.intech.tryhardninja.restAPI;

import com.intech.tryhardninja.client.OverwatchClientAPI;
import com.intech.tryhardninja.entities.OverwatchData;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class OwApiController {
    private OverwatchClientAPI overwatchClientAPI = new OverwatchClientAPI();

    @GetMapping(value="/getOwData/{playerName}")
    public Object getOwData(@PathVariable String playerName) throws IOException {
        OverwatchData mappedOverwatchData = overwatchClientAPI.getPlayerInfo(playerName);
        return mappedOverwatchData;
    }

}
